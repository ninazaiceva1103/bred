#ifndef FILE_READER_H
#define FILE_READER_H

#include "boring.h"

void read(const char* file_name, inter* array[], int& size);

#endif