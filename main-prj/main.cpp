#include <iostream>
#include "boring.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"
#include <string>
#include "output.h"


using namespace std;


int main()
{
    setlocale(LC_ALL, "rus");
    cout << "Laboratory work #8. GIT\n";

    cout << "Variant #5. Internet\n";
    cout << "Author: Zaiceva Nina\n";
    cout << "Group: 16\n";


    inter* array[MAX_FILE_ROWS_COUNT];
    int size;

    try {

        read("data.txt", array, size);


        output(array, size);
        search(array, size);


        for (int i = 0; i < size; i++) {
            delete array[i];
        }
    }
    catch (const char* error) {
        
        cout << error << '\n';
    }


    int k;
    cin >> k;

    return 0;
}