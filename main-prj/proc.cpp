#include "filter.h"
#include "boring.h"
#include <cstring>
#include <iostream>
#include <string>
#include "output.h"

bool checkz(string item, string str) {
	if (item == str) {
		return true;
	}
	return false;
}

void searchz(inter* array[], int size) {
	string str;
	cin >> str;
	times time;
	time.hour = 0;
	time.minuts = 0;
	time.seconds = 0;
	for (int i = 0; i < size; i++) {
		if (checkz(array[i]->way, str)) {
			time.hour += (array[i]->end.hour - array[i]->begin.hour);
			time.minuts += (array[i]->end.minuts - array[i]->begin.minuts);
			time.seconds += (array[i]->end.seconds - array[i]->begin.seconds);
		}
	}

	if (time.seconds < 0) {
		time.minuts--;
		time.seconds = 60 + time.seconds;
	}
	if (time.minuts < 0) {
		time.hour--;
		time.minuts = 60 + time.minuts;
	}
	if (time.seconds > 59) {
		time.minuts++;
		time.seconds -= 60;
	}
	if (time.minuts > 59) {
		time.hour++;
		time.minuts -= 60;
	}


	if (time.hour >= 0) {
		cout << time.hour << ".";
		if (time.minuts >= 0 && time.minuts < 10) {
			cout << "0" << time.minuts << ".";
		}
		else {
			cout << time.minuts << ".";
		}
		if (time.seconds >= 0 && time.seconds < 10) {
			cout << "0" << time.seconds;
		}
		else {
			cout << time.seconds;
		}
	}
	else {
		cout << "invalid input";
	}
}
