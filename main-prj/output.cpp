#include "boring.h"
#include "output.h"
#include <iostream>

using namespace std;

void out(inter* item) {
	cout << item->begin.hour << ".";
	cout << item->begin.minuts << ".";
	cout << item->begin.seconds << "  ";
	cout << item->end.hour << ".";
	cout << item->end.minuts << ".";
	cout << item->end.seconds << "   ";
    cout << item->info.in << " ";
	cout << item->info.out << "   ";
	cout << item->way << " ";
}

void output(inter* array[], int size) {
	for (int i = 0; i < size; i++) {
		out(array[i]);
		cout << endl;
	}
}