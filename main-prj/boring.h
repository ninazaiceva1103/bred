#ifndef boring_H
#define boring_H

#include "constants.h"

using namespace std;

struct times {
    int hour;
    int minuts;
    int seconds;
};
struct alldata {
    int in;
    int out;
};

struct inter {
    times begin;
    times end;
    alldata info;
    char way[MAX_STRING_SIZE];
};

#endif
