#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>
#include <string>
#include <iostream>


using namespace std;

times convert(char* str)
{
    times result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.hour = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.minuts = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.seconds = atoi(str_number);
    return result;
}


void read(const char* file_name, inter* array[], int& size) {

    std::ifstream file(file_name);
    if (file.is_open())
    {
        int i = 0;
        char tmp_buffer[MAX_STRING_SIZE];


        while (!file.eof()) {

            inter* item = new inter;

            file >> tmp_buffer;
            item->begin = convert(tmp_buffer);
            file >> tmp_buffer;
            item->end = convert(tmp_buffer);
            file >> item->info.in;
            file >> item->info.out;
            file.ignore();
            file.getline(item->way, MAX_STRING_SIZE);

            array[i] = item;
            i++;
        }
        size = i;
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}